LATEXMK = latexmk -pdflatex=lualatex -pdf

# Project specific settings
DOCNAME = cfl-av1-presentation

# Targets
all: doc
doc: pdf
pdf: cfl-av1-status.pdf cfl-av1-hw.pdf

%.indent: %.tex
	latexindent -w -l $*.tex

%.lacheck: %.indent
	lacheck $*.tex

%.chk: %.lacheck
	bash checktex.sh $*.tex

# Rules
%.pdf: %.chk
	$(LATEXMK) -pdf $*


