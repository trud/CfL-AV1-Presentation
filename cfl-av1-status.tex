\documentclass[10pt]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{pgfplots}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\captionsetup[subfigure]{labelformat=empty}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]{}
\usepackage{hyperref}
\usepackage{breakurl}
% chktex-file 4
\def\UrlBreaks{\do\/\do-}
\usepackage{textcomp}
\usepackage{textpos}
\usetikzlibrary{shapes,arrows}

\title{Chroma from Luma (CfL) in AV1}
\subtitle{Status Update}
\author{Luc Trudeau}
\institute{Mozilla and the Xiph.Org Foundation}
\date{Alliance for Open Media Working Group, April 2017}

\usetheme{metropolis}

\begin{document}

\frame{\titlepage}

\addtobeamertemplate{frametitle}{}{%
  \begin{textblock*}{200mm} (0.9\textwidth,-0.7cm)
    moz://a
  \end{textblock*}
}
\begin{frame}
  \frametitle{Chroma from Luma (CfL)}

  A coding tool that predicts information in the chromatic planes based on previously encoded information in the luma plane.

  \begin{figure}
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
      \includegraphics[width=\textwidth]{images/RedPanda_Y}
      \caption{\tiny Luminance (Luma)}\label{fig:luma}
    \end{subfigure}
    \begin{subfigure}[b]{0.32\textwidth}
      \includegraphics[width=\textwidth]{images/RedPanda_Cb}
      \caption{\tiny Blue-difference chroma plane (Cb)}\label{fig:cb}
    \end{subfigure}
    \begin{subfigure}[b]{0.32\textwidth}
      \includegraphics[width=\textwidth]{images/RedPanda_Cr}
      \caption{\tiny Red-difference chroma plane (Cr)}\label{fig:cr}
    \end{subfigure}
  \end{figure}

  CfL assumes that luma and chroma are locally correlated, this can be seen in the previous images\footnote{Notice the resemblance between luma and chroma}
\end{frame}


\begin{frame}
  \frametitle{Proposed vs.\ prior CfLs}
  \begin{table}
    \scriptsize\begin{tabular}{lcccc}
    \toprule
    & LM Mode\footnote{\tiny 	Chen et al. ``Chroma Intra Prediction by Reconstructed Luma Samples'', JCTVC-E266, \url{http://phenix.int-evry.fr/jct/doc\_end\_user/documents/5\_Geneva/wg11/JCTVC-E266-v4.zip} (March 2011) }
    & Thor CfL\footnote{\tiny Steinar Midtskogen, ``Improved chroma prediction'' IETF draft-midtskogen-netvc-chromapred-02 \url{https://tools.ietf.org/html/draft-midtskogen-netvc-chromapred-02} (October 2016)}
    & Daala CfL\footnote{\tiny Nathan E. Egge and Jean-Marc Valin, ``Predicting Chroma from Luma with Frequency Domain Intra Prediction'', \url{https://people.xiph.org/~unlord/spie_cfl.pdf} (April 2016)}
    & Proposed \\\midrule
    Prediction domain        & Spatial   & Spatial   & Frequency & Spatial  \\\midrule
    Bitstream signaling      & No        & No        & Sign bit, & Alpha    \\
    &           &            & PVQ gain  &          \\\midrule
    Activation mechanism     & LM Mode,   & Threshold & Signaled  & DC\_PRED \\
    & $4 \times 4$ and $8 \times 8 $     &           &           &          \\\midrule
    Requires PVQ             & No        & No        & Yes       & No       \\\midrule
    Encoder model fitting    & Yes       & Yes       & Via PVQ   & Yes      \\\midrule
    Decoder model fitting\footnote{\tiny When model fitting is performed in the decoder, the original chroma planes cannot be used.}
    & Yes\footnote{\tiny Complexity concerns related to model fitting during the decoding process was one of the reasons for the removal of LM Mode in HEVC.}
    & Yes       & No        & No       \\\midrule
    \end{tabular}
  \end{table}
\end{frame}

\section{Description of the Proposed CfL}
\begin{frame}
  \frametitle{Computing the CfL scaling factor (encoder only)}
  For an intra prediction block, where the uv\_mode is DC\_PRED, $\alpha$ is computed using the reconstructed luma pixels, the original chroma pixels and a prediction block level DC\_PRED\footnote{DC\_PRED is computed over the entire prediction block size, not over the transform size.}

  \begin{figure}
    \begin{tikzpicture}
      \tikzset{%
        block/.style    = {draw, rectangle, minimum height = 0.75cm,
          minimum width = 3em},
        sum/.style      = {draw, circle, inner sep=0pt, minimum size=0.5cm}, % Adder
        input/.style    = {coordinate}, % Input
        output/.style   = {coordinate} % Output
      }

      \draw
      node at (0,0)[right=-3mm]{\textopenbullet}
      node [name=input2, below=-2.5mm] at (1,0) {}
      node [input, name=input1] {}
      node at (3,0) [block] (avg) {\scriptsize{average}}
      node at (2.5,-1) [block] (conv) {\scriptsize{Downsampling\footnote{This step is required if the sequence is not 4:4:4}}}
      node at (4,-1) [sum] (sub1) {$-$}
      node at (0,-2)[right=-3mm]{\textopenbullet}
      node [input, name=input3] at(0, -2) {}
      node at (4,-2) [sum] (sub2) {$-$}
      node at (5.5,-1.5) [block] (least) {\scriptsize{Least Squares}}
      node at (0,-3)[right=-3mm]{\textopenbullet}
      node [input, name=input4] at(0, -3) {};

      \draw[->](input1) -- node[above, text width=4.5cm] {\scriptsize{Reconstructed luma pixels}}(avg);
      \draw[->](avg) -| node {} (sub1);
      \draw[->](input2) |- node {} (conv);
      \draw[->](conv) |- node {} (sub1);
      \draw[->](input3) -- node[above, text width=5cm] {\scriptsize{Original Chroma pixels}}(sub2);
      \draw[->](sub1) -| node {} (least);
      \draw[->](input4) -| node[above, text width=9cm] {\scriptsize{Chroma DC\_PRED pixels}}(sub2);
      \draw[->](sub2) -| node {} (least);
      \draw[->](least) -- (7, -1.5) node [right] {\scriptsize{$\alpha$}};

    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{CfL prediction (Encoder and Decoder)}
  The CfL prediction is computed using the reconstructed luma pixels\footnote{Resampling is required for 4:2:0 (4 adds and 1 shift per chroma pixel)}, the prediction block-level DC\_PRED and the dequantized $\alpha$ value.

  \begin{figure}
    \begin{tikzpicture}
      \tikzset{%
        block/.style    = {draw, rectangle, minimum height = 0.75cm,
          minimum width = 3em},
        sum/.style      = {draw, circle, inner sep=0pt, minimum size=0.5cm}, % Adder
        input/.style    = {coordinate}, % Input
        output/.style   = {coordinate} % Output
      }

      \draw
      node at (0,0)[right=-3mm]{\textopenbullet}
      node [name=input2, below=-2.5mm] at (1,0) {}
      node [input, name=input1] {}
      node at (3,0) [block] (avg) {\scriptsize{average}}
      node at (2.5,-1) [block] (conv) {\scriptsize{Downsampling\footnote{This step is required if the sequence is not 4:4:4}}}
      node at (4,-1) [sum] (sub1) {$-$}
      node at (0,-2)[right=-3mm]{\textopenbullet}
      node [input, name=input3] at(0, -2) {}
      node at (5,-2) [sum] (mul1) {$\times$}
      node at (6,-3) [sum] (sum1) {$+$}
      node at (0,-3)[right=-3mm]{\textopenbullet}
      node [input, name=input4] at(0, -3) {};

      \draw[->](input1) -- node[above, text width=4.5cm] {\scriptsize{Reconstructed luma pixels}}(avg);
      \draw[->](avg) -| node {} (sub1);
      \draw[->](input2) |- node {} (conv);
      \draw[->](conv) |- node {} (sub1);
      \draw[->](input3) -- node[above, text width=6cm] {\scriptsize{Signaled $\alpha$}}(mul1);
      \draw[->](sub1) -| node {} (mul1);
      \draw[->](input4) -- node[above, text width=7cm] {\scriptsize{Chroma DC\_PRED pixels}}(sum1);
      \draw[->](mul1) -| node {} (sum1);
      \draw[->](sum1) -- (7, -3) node [right] {\scriptsize{CfL prediction}};

    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Vector Quantization of $\alpha$ Cb and $\alpha$ Cr}
  Both chromatic $\alpha$s are vector quantized. The vector quantization table\footnote{The quantization table is normative, but could be added to the bitstream} is trained using $k$-means (Lloyd's algorithm).
  \begin{figure}
    \centering
    \includegraphics[width=0.55\textwidth]{images/voronoi.png}\label{fig:voronoi}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Signaling}
  Both $\alpha_{Cb}$ and $\alpha_{Cr}$ are combined into a symbol of a 16 value CDF

  The sign of each $\alpha$ is signaled using a bit\footnote{Both signs could be signaled using a single symbol}

  \begin{figure}
    \begin{tikzpicture}
      \tikzset{%
        block/.style    = {draw, rectangle, minimum height = 0.5cm,
          minimum width = 3em},
        sum/.style      = {draw, diamond, inner sep=0pt, minimum size=0.5cm}, % Adder
        input/.style    = {coordinate}, % Input
        output/.style   = {coordinate} % Output
      }

      \draw
      node at (0,-0.5)[below=-3mm]{\textopenbullet}
      node at (0,-1) [sum] (skip1) {}
      node at (-1,-1.75)[below=-3mm](out1) {\textopenbullet}
      node at (1.5,-1.75) [block] (read1) {\tiny{ind = aom\_read\_symbol()}}
      node at (1.5,-2.5) [sum] (skip2) {}
      node at (2.5,-3.25) [block] (read2) {\tiny{sign[0] = aom\_read\_bit()}}
      node at (2.5,-4.25) [sum] (skip3) {}
      node at (3.5,-5) [block] (read3) {\tiny{sign[1] = aom\_read\_bit()}}
      node at (3.5,-6)[below=-3mm](out2) {\textopenbullet};

      \draw[->](0,-0.5) -- node[above] {}(skip1);
      \draw[->](skip1) -| node[above] {\tiny{block skipped}} (-1, -1.57);
      \draw[->](skip1) -| node[above] {\tiny{mbmi$\rightarrow$skipped == 0}} (read1);
      \draw[->](read1) -- node[above] {}(skip2);
      \draw[->](skip2) -| node[above right] {\tiny{cfl\_alpha\_codes[ind][0] != 0}} (read2);
      \draw(skip2) -| (0.5, -3.25) {};
      \draw[->](read2) -- node[above] {}(skip3);
      \draw[->](0.5, -3.25) |- (2.5, -3.75);
      \draw[->](skip3) -| node[above right] {\tiny{cfl\_alpha\_codes[ind][1] != 0}} (read3);
      \draw(skip3) -| (1.5, -5) {};
      \draw[->](read3) -- node[above] {} (3.5, -5.82);
      \draw[->](1.5, -5) |- (3.5, -5.5);

    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Results (AWCY)}

  AV1 with default experiments enabled vs. AV1 with CfL

  Dataset: Subset 1 (50 4:2:0 still images)

  \begin{table}
    \scriptsize\begin{tabular}{rrrrrrr}
    \multicolumn{7}{c}{BD-Rate with respect to} \\

    \toprule
    PSNR &  PSNR Cb &  PSNR Cr & PSNR HVS &   SSIM & MS SSIM & CIEDE 2000\footnote{CIEDE2000 is the only metric that considers both luma and chroma planes} \\\midrule
    1.2589 & -15.5532 & -13.6577 &   1.4392 & 1.3205 &  1.2999 & \textbf{-4.4521} \\\bottomrule
    \end{tabular}
  \end{table}
  % chktex-file 8
  \tiny{\url{https://arewecompressedyet.com/?job=master\%402017-03-27T18\%3A41\%3A56.236Z\&job=CfL\_Double\_DC\_PRED\%402017-04-15T01\%3A48}}
\end{frame}
\section{Examples}
\begin{frame}
  \frametitle{AV1}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{images/av1_cb.png}
  \end{figure}
  \tiny{Plane: Blue-difference chroma plane (Cb)\newline
    PSNR Cb:\@ 41.0378 dB\newline
    QP=55\newline Sequence: Washington\_Monument,\_Washington,\_D.C.\_04037u\_original.y4m (subset1)
    \newline Analyzer link: \url{https://goo.gl/69N6LC}}
\end{frame}

\begin{frame}
  \frametitle{AV1 + CfL}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{images/cfl_cb.png}\label{fig:cfl}
  \end{figure}
  \tiny{Plane: Blue-difference chroma plane (Cb)\newline
    PSNR Cb:\@ 42.9614 dB\newline
    QP=55\newline Sequence: Washington\_Monument,\_Washington,\_D.C.\_04037u\_original.y4m (subset1)
    \newline Analyzer link: \url{https://goo.gl/69N6LC}}
\end{frame}

\begin{frame}
  \frametitle{AV1}
  \begin{figure}
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
      \includegraphics[width=\textwidth]{images/av1_2_cb.png}
      \caption{PSNR Cb: 40.9118 dB}
    \end{subfigure}
    \begin{subfigure}[b]{0.48\textwidth}
      \includegraphics[width=\textwidth]{images/av1_2_cr.png}
      \caption{PSNR Cr: 41.3498 dB}
    \end{subfigure}
  \end{figure}
  \tiny{QP=55\newline Sequence: US\_Navy\_111117-N-UB993-082\_A\_Sailor\_examines\_a\_patient\_during\_drill.y4m (subset1)
    \newline Analyzer link: \url{https://goo.gl/NdLyzu}}
\end{frame}

\begin{frame}
  \frametitle{AV1 + CfL}
  \begin{figure}
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
      \includegraphics[width=\textwidth]{images/cfl_2_cb.png}
      \caption{PSNR Cb: 41.9660 dB}
    \end{subfigure}
    \begin{subfigure}[b]{0.48\textwidth}
      \includegraphics[width=\textwidth]{images/cfl_2_cr.png}
      \caption{PSNR Cr: 42.4291 dB}
    \end{subfigure}
  \end{figure}
  \tiny{QP=55\newline Sequence: US\_Navy\_111117-N-UB993-082\_A\_Sailor\_examines\_a\_patient\_during\_drill.y4m (subset1)
    \newline Analyzer link: \url{https://goo.gl/NdLyzu}}
\end{frame}

\section{Future Work}
\begin{frame}
  \frametitle{Work remaining before proposal for adoption}
  \begin{itemize}
    \item Improve quantization tables and probability tables
    \item Optimize code book size
    \item Optimize encoder $\alpha$ selection to improve coding efficiency
    \item Use multisymbol to signal sign bits
    \item 4:2:2 support
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Ongoing research}
  \begin{itemize}
    \item Tuning the luma-chroma balance to, e.g., reduce or eliminate the loss in luma PSNR (for a smaller CIEDE gain)
    \item Add the code book to the bitstream
    \item CfL in inter (similar to Thor)
  \end{itemize}
\end{frame}


\end{document}
